import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import pw.elka.tkom.parser.Parser;
import pw.elka.tkom.parser.Scanner;

/**
 * Created by mmudel on 22.04.2016.
 */
public class Main {
    public static void main(String[] args) {
//        System.out.println("Enter path for data (json)");
//        String json = System.console().readLine();
//        System.out.println("Enter path for template (tmpl)");
//        String template = System.console().readLine();
        System.out.println("Searching for files data.json and view.tmpl in local directory..");
//        Path jsonPath = Paths.get(json);
//        Path templatePath = Paths.get(template);
        Path jsonPath = Paths.get("data.json");
        Path templatePath = Paths.get("view.tmpl");
        Map<String, ArrayList<Map<String, Object>>> result = new HashMap<>();
        InputStreamReader reader;
        ObjectMapper mapper = new ObjectMapper();
        try {
            reader = new InputStreamReader(Files.newInputStream(templatePath));
        } catch (Exception e) {
            System.out.println("exception");
            System.out.println(e.toString());
            return;
        }
        try {
            result = mapper.readValue(jsonPath.toFile(), new TypeReference<Map<String, Object>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader bufferedReader = new BufferedReader(reader);
        Scanner scanner = new Scanner(bufferedReader);
        Parser parser = new Parser(scanner);
        try {
            parser.run();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("index.html", "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        writer.print(parser.generatedFile);
        writer.close();
    }
}
