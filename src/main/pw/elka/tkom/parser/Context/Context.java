package pw.elka.tkom.parser.Context;

import pw.elka.tkom.parser.ast.CommandFunction;
import pw.elka.tkom.parser.ast.IValue;
import pw.elka.tkom.parser.ast.Identifier;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mmudel on 12.06.2016.
 */
public class Context {
    public Map<String, IValue> valueMap = new HashMap<>();
    public Map<String, CommandFunction> functionMap = new HashMap<>();
}
