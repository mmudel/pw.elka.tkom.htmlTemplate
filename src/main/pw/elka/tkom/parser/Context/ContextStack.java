package pw.elka.tkom.parser.Context;

import pw.elka.tkom.parser.ast.CommandFunction;
import pw.elka.tkom.parser.ast.IValue;
import pw.elka.tkom.parser.ast.Identifier;

import java.util.Stack;

/**
 * Created by mmudel on 13.06.2016.
 */
public class ContextStack {
    private Stack<Context> contexts;

    public ContextStack(){
        contexts = new Stack<>();
    }

    public void addScope(){
        Context context = new Context();
        contexts.push(context);
    }
    
    public void deleteScope(){
        contexts.pop();
    }
    
    public void addValue(Identifier identifier, IValue value){
        Context context = contexts.pop();
        context.valueMap.put(identifier.getValue(), value);
        contexts.push(context);
    }
    
    public void addFunction(Identifier identifier, CommandFunction function){
        Context context = contexts.pop();
        context.functionMap.put(identifier.getValue(), function);
        contexts.push(context);
    }
    
    public IValue findValue(Identifier identifier){
        IValue value;
        Stack<?> contextStack = (Stack<?>) this.contexts.clone();
        for (int i = 0; i < contexts.size(); i++) {
            Context context = (Context) contextStack.pop();
            if((value = context.valueMap.get(identifier.getValue())) != null){
                return value;
            }
        }
        return null;
    }

    public CommandFunction findFunction(Identifier identifier){
        CommandFunction commandFunction;
        Stack<?> contextStack = (Stack<?>) this.contexts.clone();
        for (int i = 0; i < contexts.size(); i++) {
            Context context = (Context) contextStack.pop();
            if((commandFunction = context.functionMap.get(identifier.getValue())) != null){
                return commandFunction;
            }
        }
        return null;
    }


}
