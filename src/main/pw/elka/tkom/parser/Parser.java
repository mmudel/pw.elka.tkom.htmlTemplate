package pw.elka.tkom.parser;

import pw.elka.tkom.parser.Context.ContextStack;
import pw.elka.tkom.parser.ast.*;
import pw.elka.tkom.parser.exceptions.UnexpectedInputException;
import pw.elka.tkom.parser.exceptions.UnknownTokenException;
import pw.elka.tkom.parser.tokens.AdditiveOperator;
import pw.elka.tkom.parser.tokens.ComparisonOperator;
import pw.elka.tkom.parser.tokens.MultiplicativeOperator;
import pw.elka.tkom.parser.tokens.Token;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mmudel on 29.05.2016.
 */
public class Parser {
    Scanner scanner;

    public Parser(Scanner scanner) {
        this.scanner = scanner;
    }

    public String generatedFile;
    public ContextStack contextStack;

    public void run() throws UnknownTokenException, UnexpectedInputException {
        contextStack = new ContextStack();
        StringBuilder stringBuilder = new StringBuilder();
        CommandList commandList = parse();
        commandList.generate(contextStack, stringBuilder);
        this.generatedFile = stringBuilder.toString();
        return;
    }

    public CommandList parse() throws UnexpectedInputException, UnknownTokenException {
        advance();
        return parseCommandList();
    }

    public CommandShould parseCommandShould() throws UnexpectedInputException, UnknownTokenException {
        readToken(Token.TokenType.Should);
        readToken(Token.TokenType.OpeningParenthesis);
        BooleanExpression booleanExpression = parseBooleanExpression();
        readToken(Token.TokenType.Coma);
        Identifier identifier = new Identifier(readToken(Token.TokenType.Identifier).getStringValue());
        readToken(Token.TokenType.ClosingParenthesis);
        readToken(Token.TokenType.CloseCommand);
        return new CommandShould(booleanExpression, identifier);
    }

    public CommandIf parseCommandIf() throws UnexpectedInputException, UnknownTokenException {
        readToken(Token.TokenType.If);
        readToken(Token.TokenType.OpeningParenthesis);
        BooleanExpression booleanExpression = parseBooleanExpression();
        readToken(Token.TokenType.ClosingParenthesis);
        readToken(Token.TokenType.CloseCommand);
        CommandList commandList = parseCommandList();
        readToken(Token.TokenType.Endif);
        readToken(Token.TokenType.CloseCommand);
        return new CommandIf(booleanExpression, commandList);
    }

    public CommandForeach parseCommandForeach() throws UnexpectedInputException, UnknownTokenException {
        readToken(Token.TokenType.Foreach);
        readToken(Token.TokenType.OpeningParenthesis);
        Identifier identifier = new Identifier(readToken(Token.TokenType.Identifier).getStringValue());
        readToken(Token.TokenType.In);
        JsonObject jsonObject = parseJsonObject();
        readToken(Token.TokenType.ClosingParenthesis);
        readToken(Token.TokenType.CloseCommand);
        CommandList commandList = parseCommandList();
        readToken(Token.TokenType.Endfor);
        readToken(Token.TokenType.CloseCommand);
        return new CommandForeach(commandList, identifier, jsonObject);
    }

    public CommandList parseCommandList() throws UnknownTokenException, UnexpectedInputException {
        List<ICommand> commands = new LinkedList<>();
        ICommand command;
        while (!checkTokenType(Token.TokenType.EOF)) {
            if (checkTokenType(Token.TokenType.OpenCommand)) {
                readToken(Token.TokenType.OpenCommand);
                command = parseInstruction();
                if(command != null) {
                    commands.add(command);
                } else {
                    break;
                }
            } else {
                commands.add(new TextWithoutInterpretation(readToken(Token.TokenType.TextWithoutInterpretation).getStringValue()));
            }
        }
        return new CommandList(commands);
    }

    private ICommand parseInstruction() throws UnexpectedInputException, UnknownTokenException {
        Token token = requireToken(Arrays.asList(Token.TokenType.Should, Token.TokenType.If, Token.TokenType.Identifier, Token.TokenType.Foreach, Token.TokenType.Function, Token.TokenType.Print,
                Token.TokenType.Endfor, Token.TokenType.Return, Token.TokenType.Endif));
        switch (token.tokenType) {
            case Should:
                return parseCommandShould();
            case If:
                return parseCommandIf();
            case Identifier:
                Identifier identifier = new Identifier(readToken(Token.TokenType.Identifier).getStringValue());
                if(checkTokenType(Token.TokenType.AssignmentOperator)){
                    return parseAssignmentWithIdentifier(identifier);
                } else {
                    return parseCommandFuncCallWithIdentifier(identifier);
                }
            case Foreach:
                return parseCommandForeach();
            case Function:
                return parseCommandFunction();
            case Print:
                return parseCommandPrint();
            default:
                return null;
        }
    }

    public ICommand parseAssignmentWithIdentifier(Identifier identifier) throws UnknownTokenException, UnexpectedInputException {
        readToken(Token.TokenType.AssignmentOperator);
        IArithmeticExpression arithmeticExpression = parseAdditiveExpression();
        readToken(Token.TokenType.CloseCommand);
        return new Assignment(identifier, arithmeticExpression);
    }

    public ICommand parseCommandFunction() throws UnknownTokenException, UnexpectedInputException {
        readToken(Token.TokenType.Function);
        NumericValue value = null;
        Identifier identifier = new Identifier(readToken(Token.TokenType.Identifier).getStringValue());
        readToken(Token.TokenType.OpeningParenthesis);
        List<Identifier> arguments = new LinkedList<>();
        if (checkTokenType(Token.TokenType.Identifier)) {
            arguments.add(new Identifier(readToken(Token.TokenType.Identifier).getStringValue()));
            while (checkTokenType(Token.TokenType.Coma)) {
                readToken(Token.TokenType.Coma);
                arguments.add(new Identifier(readToken(Token.TokenType.Identifier).getStringValue()));
            }
        }
        readToken(Token.TokenType.ClosingParenthesis);
        readToken(Token.TokenType.CloseCommand);
        CommandList commandList = parseCommandList();
        readToken(Token.TokenType.Return);
        if (!checkTokenType(Token.TokenType.CloseCommand)) {
            value = parseNumeric();
        }
        readToken(Token.TokenType.CloseCommand);
        return new CommandFunction(identifier, arguments, commandList, value);
    }

    public CommandPrint parseCommandPrint() throws UnexpectedInputException, UnknownTokenException {
        List<IValue> values = new LinkedList<>();
        readToken(Token.TokenType.Print);
        readToken(Token.TokenType.OpeningParenthesis);
        values.add(parseValue());
        while (checkTokenType(Token.TokenType.Coma)) {
            readToken(Token.TokenType.Coma);
            values.add(parseValue());
        }
        readToken(Token.TokenType.ClosingParenthesis);
        readToken(Token.TokenType.CloseCommand);
        return new CommandPrint(values);
    }

    public Value parseValue() throws UnexpectedInputException, UnknownTokenException {
        Value value;
        Token token = requireToken(Arrays.asList(Token.TokenType.Identifier, Token.TokenType.StringValue, Token.TokenType.NumericValue));
        if (token.tokenType.equals(Token.TokenType.Identifier)) {
            value = new Value(parseJsonObject());
        } else if (token.tokenType.equals(Token.TokenType.StringValue)) {
            value = new Value(readToken(Token.TokenType.StringValue).getStringValue());
        } else {
            value = new Value(readToken(Token.TokenType.NumericValue).getNumberValue());
        }
        return value;
    }

    public BooleanExpression parseBooleanExpression() throws UnexpectedInputException, UnknownTokenException {
        IArithmeticExpression arithmeticExpressionLeft = parseAdditiveExpression();
        ComparisonOperator comparisonOperator = readToken(Token.TokenType.ComparisonOperator).getComparisonOperator();
        IArithmeticExpression arithmeticExpressionRight = parseAdditiveExpression();
        return new BooleanExpression(arithmeticExpressionLeft, arithmeticExpressionRight, comparisonOperator);
    }

    public IArithmeticExpression parseAdditiveExpression() throws UnexpectedInputException, UnknownTokenException {
        IArithmeticExpression expression = parseMultiplicativeExpression();
        while (checkTokenType(Token.TokenType.AdditiveOperator)) {
            AdditiveOperator op = readToken(Token.TokenType.AdditiveOperator).getAdditiveOperator();
            expression = new AdditiveExpression(expression, parseMultiplicativeExpression(), op);
        }
        return expression;
    }

    public IArithmeticExpression parseMultiplicativeExpression() throws UnexpectedInputException, UnknownTokenException {
        IArithmeticExpression expression = parsePrimary();
        while (checkTokenType(Token.TokenType.MultiplicativeOperator)) {
            MultiplicativeOperator op = readToken(Token.TokenType.MultiplicativeOperator).getMultiplicativeOperator();
            expression = new MultiplicativeExpression(expression, parsePrimary(), op);
        }
        return expression;
    }

    public JsonObject parseJsonObject() throws UnknownTokenException, UnexpectedInputException {
        List<Identifier> identifiers = new LinkedList<>();
        identifiers.add(new Identifier(readToken(Token.TokenType.Identifier).getStringValue()));
        while (checkTokenType(Token.TokenType.Dot)) {
            readToken(Token.TokenType.Dot);
            identifiers.add(new Identifier(readToken(Token.TokenType.Identifier).getStringValue()));
        }
        JsonObject jsonObject = new JsonObject(identifiers);
        return jsonObject;
    }

    public JsonObject parseJsonObjectWithIdentifier(Identifier identifier) throws UnknownTokenException, UnexpectedInputException {
        List<Identifier> identifiers = new LinkedList<>();
        identifiers.add(identifier);
        while (checkTokenType(Token.TokenType.Dot)) {
            readToken(Token.TokenType.Dot);
            identifiers.add(new Identifier(readToken(Token.TokenType.Identifier).getStringValue()));
        }
        JsonObject jsonObject = new JsonObject(identifiers);
        return jsonObject;
    }

    public IArithmeticExpression parsePrimary() throws UnknownTokenException, UnexpectedInputException {
        IArithmeticExpression arithmeticExpression = null;
        Token token = requireToken(Arrays.asList(Token.TokenType.OpeningParenthesis, Token.TokenType.Identifier, Token.TokenType.NumericValue, Token.TokenType.Minus));
        switch (token.tokenType) {
            case OpeningParenthesis:
                readToken(Token.TokenType.OpeningParenthesis);
                if(checkTokenType(Token.TokenType.Minus)) {
                    arithmeticExpression = parsePrimary();
                    arithmeticExpression.changeNegative();
                }
                else {
                    arithmeticExpression = parseAdditiveExpression();
                }
                readToken(Token.TokenType.ClosingParenthesis);
                break;
            case Identifier:
                Identifier identifier = new Identifier(readToken(Token.TokenType.Identifier).getStringValue());
                if(checkTokenType(Token.TokenType.OpeningParenthesis)){
                    arithmeticExpression = parseArithmeticFuncCallWithIdentifier(identifier);
                } else {
                    arithmeticExpression = parseNumericWithIdentifier(identifier);
                }
                break;
            case NumericValue:
                arithmeticExpression = parseNumeric();
                break;
        }
        return arithmeticExpression;
    }

    public NumericValue parseNumeric() throws UnknownTokenException, UnexpectedInputException {
        Token token = requireToken(Arrays.asList(Token.TokenType.Identifier, Token.TokenType.NumericValue));
        if (token.tokenType == Token.TokenType.Identifier) {
            JsonObject jsonObject = parseJsonObject();
            return new NumericValue(jsonObject);
        } else {
            return new NumericValue(readToken(Token.TokenType.NumericValue).getNumberValue());
        }
    }

    public NumericValue parseNumericWithIdentifier(Identifier identifier) throws UnknownTokenException, UnexpectedInputException {
        JsonObject jsonObject = parseJsonObjectWithIdentifier(identifier);
        return new NumericValue(jsonObject);
    }

    public ArithmeticFuncCall parseArithmeticFuncCallWithIdentifier(Identifier identifier) throws UnknownTokenException, UnexpectedInputException {
        List<IValue> args = new LinkedList<>();
        readToken(Token.TokenType.OpeningParenthesis);
        if (checkTokenType(Token.TokenType.Identifier) || checkTokenType(Token.TokenType.StringValue) || checkTokenType(Token.TokenType.NumericValue)) {
            args.add(parseValue());
            while (checkTokenType(Token.TokenType.Coma)) {
                readToken(Token.TokenType.Coma);
                args.add(parseValue());
            }
        }
        readToken(Token.TokenType.ClosingParenthesis);

        return new ArithmeticFuncCall(identifier, args);
    }

    public CommandFuncCall parseCommandFuncCallWithIdentifier(Identifier identifier) throws UnknownTokenException, UnexpectedInputException {
        List<IValue> args = new LinkedList<>();
        readToken(Token.TokenType.OpeningParenthesis);
        if (checkTokenType(Token.TokenType.Identifier) || checkTokenType(Token.TokenType.StringValue) || checkTokenType(Token.TokenType.NumericValue)) {
            args.add(parseValue());
            while (checkTokenType(Token.TokenType.Coma)) {
                readToken(Token.TokenType.Coma);
                args.add(parseValue());
            }
        }
        readToken(Token.TokenType.ClosingParenthesis);
        readToken(Token.TokenType.CloseCommand);

        return new CommandFuncCall(identifier, args);
    }


    private Token requireToken(List<Token.TokenType> expectedTokens) throws UnexpectedInputException {
        Token token = scanner.getToken();
        if (!expectedTokens.contains(token.tokenType)) {
            throwOnUnexpectedInput(expectedTokens);
        }
        return token;
    }

    private Token readToken(Token.TokenType tokenType) throws UnexpectedInputException, UnknownTokenException {
        Token newToken = requireToken(Arrays.asList(tokenType));
        advance();
        return newToken;
    }

    private boolean checkTokenType(Token.TokenType expectedToken) {
        return scanner.getToken().tokenType.equals(expectedToken);
    }

    private void advance() throws UnknownTokenException {
        scanner.readNextToken();
    }

    private void throwOnUnexpectedInput(List<Token.TokenType> expectedTokens) throws UnexpectedInputException {
        StringBuilder stringBuilder = new StringBuilder("Unexpected token: " + scanner.getToken().tokenType + " at line: "
                + scanner.lineNumber + ", ");
        stringBuilder.append("expecting: " + expectedTokens.get(0));
        for (int i = 1; i < expectedTokens.size(); i++) {
            stringBuilder.append(" or " + expectedTokens.get(i));
        }
        stringBuilder.append(".");
        throw new UnexpectedInputException(stringBuilder.toString());
    }
}

//zostawiam {$! przy endforeach (etc) poniewaz:
// nie moge usuwac z listy bo endforeach to nie jest command (i nie chcę żeby był)
// nie moge sprawdzac w command list bo command list nie wie w obrebie jakiej funkcji szuka (czy foreach czy if)
// nie moge zrobic oddzielnego tokena na caly endforeach poniewaz gryzlo by sie to z opencommand (prawdopodobnie trzeba by sprawdzac wczesniej czy endforeach, nieladnie)

//zostawiam % przy func call poniewaz:
//  nie moge wczytac identyfikatora i sprawdzac jaki znak jest kolejny bo strace ten identyfikator (scanner sie przesunie)

//nie mozna przypisywac obiektow, przypisanie do czegos obiektu przypisze tylko jego wartosc liczbowa

//dlaczego nie pozwalam przypisywac obiektow? bo to byloby niejednoznaczne, zalozenie jest takie ze ta czesc po prawej stronie jest liczba,
// wiec zawsze przypisuje to jako liczbe, skad mialbym wiedziec czy chce to przypisac jako obiekt czy jako liczbe/wartosc(string tez)?
// musialbym robic oddzielna obsluge (nie arithmetic.calculate())

// jak zrobic to parsowanie jsona zeby bylo okay
// jak zaimplementowac iterator