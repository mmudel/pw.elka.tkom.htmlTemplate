package pw.elka.tkom.parser;

import pw.elka.tkom.parser.exceptions.UnknownTokenException;
import pw.elka.tkom.parser.tokens.AdditiveOperator;
import pw.elka.tkom.parser.tokens.ComparisonOperator;
import pw.elka.tkom.parser.tokens.MultiplicativeOperator;
import pw.elka.tkom.parser.tokens.Token;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;

/**
 * Created by mmudel on 02.05.2016.
 */

public class Scanner {

    /* pw.elka.tkom.parser.tokens.Token at which we are currently */
    Token token;

    Reader reader;

    HashMap<String, Token.TokenType> commandsMap;

    /* Line number at which we are currently */
    int lineNumber = 1;

    public Scanner(Reader reader) {
        this.reader = reader;
        this.commandsMap = new HashMap<>();
        this.commandsMap.put("should", Token.TokenType.Should);
        this.commandsMap.put("if", Token.TokenType.If);
        this.commandsMap.put("endif", Token.TokenType.Endif);
        this.commandsMap.put("foreach", Token.TokenType.Foreach);
        this.commandsMap.put("print", Token.TokenType.Print);
        this.commandsMap.put("endfor", Token.TokenType.Endfor);
        this.commandsMap.put("function", Token.TokenType.Function);
        this.commandsMap.put("endfunction", Token.TokenType.Endfunction);
        this.commandsMap.put("return", Token.TokenType.Return);
    }

    public void readNextToken() throws UnknownTokenException {
        try {
            if (inCommandMode) {
                this.ignoreWhitespaces();
                if (tryEof()
                        || tryCloseCommand()
                        || tryComa()
                        || tryIn()
                        || tryMinus()
                        || tryDot()
                        || tryAssignmentOperator()
                        || tryCommandOrIdentifier()
                        || tryOpeningParenthesis()
                        || tryClosingParenthesis()
                        || tryComparisonOperator()
                        || tryNumericValue()
                        || tryStringValue()
                        || tryAdditiveOperator()
                        || tryMultiplicativeOperator())
                    return;
                throwUnknownTokenException(reader.read());
            } else {
                if (tryEof()
                        || tryOpenCommand()
                        || tryTextWithoutInterpretation())
                    return;
                throwUnknownTokenException(reader.read());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Token getToken() {
        return token;
    }

    /* Boolean variable telling if we are in command block */
    boolean inCommandMode = false;

    void ignoreWhitespaces() {
        int a;
        try {
            this.reader.mark(1);
            while (Character.isWhitespace(a = this.reader.read())) {
                if (a == '\n')
                    this.lineNumber++;
                this.reader.mark(1);
            }
            this.reader.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean tryEof() throws IOException {
        this.reader.mark(1);
        if (this.reader.read() == -1) {
            this.token = new Token(Token.TokenType.EOF);
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryOpenCommand() throws IOException {
        this.reader.mark(3);
        if (this.reader.read() == 123) {
            if (this.reader.read() == 36) {
                if(this.reader.read() == 32) {
                    this.token = new Token(Token.TokenType.OpenCommand);
                    this.inCommandMode = true;
                    return true;
                } else {
                    this.reader.reset();
                    return false;
                }
            } else {
                this.reader.reset();
                return false;
            }
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryCloseCommand() throws IOException {
        this.reader.mark(2);
        if (this.reader.read() == 36) {
            if (this.reader.read() == 125) {
                this.token = new Token(Token.TokenType.CloseCommand);
                this.inCommandMode = false;
                return true;
            } else {
                this.reader.reset();
                return false;
            }
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryCommandOrIdentifier() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        String command;
        Token.TokenType nextTokenType;
        int a;
        this.reader.mark(1);
        if (Character.isLetter(a = this.reader.read())) {
            stringBuilder.append(String.valueOf((char) a));
            while (Character.isLetterOrDigit(a = this.reader.read())) {
                this.reader.mark(1);
                stringBuilder.append(String.valueOf((char) a));
            }
            this.reader.reset();
            command = stringBuilder.toString();
            if ((nextTokenType = this.commandsMap.get(command)) != null) {
                this.token = new Token(nextTokenType);
            } else {
                this.token = new Token(Token.TokenType.Identifier);
                this.token.setStringValue(command);
            }
            return true;
        }
        this.reader.reset();
        return false;
    }

    private boolean tryIn() throws IOException {
        char[] nextToken = new char[2];
        this.reader.mark(2);
        this.reader.read(nextToken, 0, 2);
        if (String.valueOf(nextToken).equals("in")) {
            this.token = new Token(Token.TokenType.In);
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryOpeningParenthesis() throws IOException {
        int a;
        this.reader.mark(1);
        a = this.reader.read();
        if (String.valueOf((char) a).equals("(")) {
            this.token = new Token(Token.TokenType.OpeningParenthesis);
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryClosingParenthesis() throws IOException {
        int a;
        this.reader.mark(1);
        a = this.reader.read();
        if (String.valueOf((char) a).equals(")")) {
            this.token = new Token(Token.TokenType.ClosingParenthesis);
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryComa() throws IOException {
        int a;
        this.reader.mark(1);
        a = this.reader.read();
        if (String.valueOf((char) a).equals(",")) {
            this.token = new Token(Token.TokenType.Coma);
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryDot() throws IOException {
        int a;
        this.reader.mark(1);
        a = this.reader.read();
        if (String.valueOf((char) a).equals(".")) {
            this.token = new Token(Token.TokenType.Dot);
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryMinus() throws IOException {
        int a;
        this.reader.mark(1);
        a = this.reader.read();
        if (String.valueOf((char) a).equals("-")) {
            this.token = new Token(Token.TokenType.Minus);
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryComparisonOperator() throws IOException {
        this.reader.mark(2);
        switch (this.reader.read()) {
            case '>':
                this.reader.mark(1);
                this.token = new Token(Token.TokenType.ComparisonOperator);
                if (this.reader.read() == '=') {
                    this.token.setComparisonOperator(ComparisonOperator.IsBiggerEqual);
                    return true;
                } else {
                    this.reader.reset();
                    this.token.setComparisonOperator(ComparisonOperator.IsBigger);
                    return true;
                }
            case '<':
                this.reader.mark(1);
                this.token = new Token(Token.TokenType.ComparisonOperator);
                if (this.reader.read() == '=') {
                    this.token.setComparisonOperator(ComparisonOperator.IsLowerEqual);
                    return true;
                } else {
                    this.reader.reset();
                    this.token.setComparisonOperator(ComparisonOperator.IsLower);
                    return true;
                }
            case '=':
                if (this.reader.read() == '=') {
                    this.token = new Token(Token.TokenType.ComparisonOperator);
                    this.token.setComparisonOperator(ComparisonOperator.IsEqual);
                    return true;
                } else {
                    this.reader.reset();
                    return false;
                }
            default:
                this.reader.reset();
                return false;
        }
    }

    private boolean tryAssignmentOperator() throws IOException {
        this.reader.mark(2);
        if (this.reader.read() == '=') {
            if(this.reader.read() == 32) {
                this.token = new Token(Token.TokenType.AssignmentOperator);
                return true;
            } else {
                this.reader.reset();
                return false;
            }
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryNumericValue() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        int a;
        this.reader.mark(1);
        if (Character.isDigit(a = this.reader.read())) {
            this.reader.mark(1);
            stringBuilder.append(String.valueOf((char) a));
            while (Character.isDigit(a = this.reader.read())) {
                this.reader.mark(1);
                stringBuilder.append(String.valueOf((char) a));
            }
            this.reader.reset();
            this.token = new Token(Token.TokenType.NumericValue);
            this.token.setNumberValue(Integer.valueOf(stringBuilder.toString()));
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryStringValue() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        int a;
        this.reader.mark(1);
        if (this.reader.read() == '"') {
            while ((a = this.reader.read()) != '"') {
                stringBuilder.append(String.valueOf((char) a));
            }
            this.token = new Token(Token.TokenType.StringValue);
            this.token.setStringValue(stringBuilder.toString());
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryAdditiveOperator() throws IOException {
        this.reader.mark(1);
        int a;
        if ((a = this.reader.read()) == '+') {
            this.token = new Token(Token.TokenType.AdditiveOperator);
            this.token.setAdditiveOperator(AdditiveOperator.Additive);
            return true;
        } else if (a == '-') {
            this.token = new Token(Token.TokenType.AdditiveOperator);
            this.token.setAdditiveOperator(AdditiveOperator.Subtractive);
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryMultiplicativeOperator() throws IOException {
        this.reader.mark(1);
        int a;
        if ((a = this.reader.read()) == '*') {
            this.token = new Token(Token.TokenType.MultiplicativeOperator);
            this.token.setMultiplicativeOperator(MultiplicativeOperator.Multiplicative);
            return true;
        } else if (a == '/') {
            this.token = new Token(Token.TokenType.MultiplicativeOperator);
            this.token.setMultiplicativeOperator(MultiplicativeOperator.Divisional);
            return true;
        } else {
            this.reader.reset();
            return false;
        }
    }

    private boolean tryTextWithoutInterpretation() throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        boolean quoting = false;
        this.readAtom(stringBuilder);
        while (true) {
            if (checkNewLine(stringBuilder))
                this.lineNumber++;
            if (!quoting && checkIfQuoting(stringBuilder)) {
                quoting = true;
                this.readAtomWithoutInterpretation(stringBuilder);
            } else {
                quoting = false;
                if (!(this.readAtom(stringBuilder))) {
                    break;
                }
            }
        }

    if(stringBuilder.length()==0)
            return false;
    this.token = new Token(Token.TokenType.TextWithoutInterpretation);
    this.token.setStringValue(stringBuilder.toString());
    return true;
}

    private boolean readAtom(StringBuilder stringBuilder) throws IOException {
        this.reader.mark(2);
        char symbols[] = new char[2];
        int symbol;
        this.reader.read(symbols, 0, 2);
        if (symbols[0] == '{') {
            if (symbols[1] == '$') {
                this.reader.reset();
                return false;
            } else if (symbols[1] == -1) {
                this.reader.reset();
                symbol = this.reader.read();
                stringBuilder.append(String.valueOf((char) symbol));
                return false;
            } else {
                this.reader.reset();
                symbol = this.reader.read();
                stringBuilder.append(String.valueOf((char) symbol));
                return true;
            }
        } else {
            this.reader.reset();
            return readAtomWithoutInterpretation(stringBuilder);
        }
    }

    private boolean readAtomWithoutInterpretation(StringBuilder stringBuilder) throws IOException {
        this.reader.mark(1);
        int symbol = this.reader.read();
        if (symbol == -1) {
            this.reader.reset();
            return false;
        } else {
            stringBuilder.append(String.valueOf((char) symbol));
        }
        return true;
    }

    private boolean checkIfQuoting(StringBuilder stringBuilder) {
        if (stringBuilder.charAt(stringBuilder.length() - 1) == '/') {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            return true;
        } else {
            return false;
        }
    }

    private boolean checkNewLine(StringBuilder stringBuilder) {
        if (stringBuilder.charAt(stringBuilder.length() - 1) == '\n') {
            return true;
        } else {
            return false;
        }
    }

    private void throwUnknownTokenException(int token) throws UnknownTokenException {
        char tokenChar = (char) token;
        throw new UnknownTokenException("Unknown token " + tokenChar + " at line " + lineNumber);
    }
}