package pw.elka.tkom.parser.ast;


import pw.elka.tkom.parser.Context.Context;
import pw.elka.tkom.parser.Context.ContextStack;
import pw.elka.tkom.parser.tokens.AdditiveOperator;

/**
 * Created by mmudel on 29.05.2016.
 */
public class AdditiveExpression implements IArithmeticExpression {

    public AdditiveExpression(IArithmeticExpression leftOperand, IArithmeticExpression rightOperand, AdditiveOperator operator) {
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
        this.operator = operator;
        this.negative = false;
    }


    private boolean negative;
    private IArithmeticExpression leftOperand;
    private IArithmeticExpression rightOperand;
    private AdditiveOperator operator;

    public double calculate(ContextStack contextStack, StringBuilder stringBuilder) {
        double result = operator.calculate(leftOperand.calculate(contextStack, stringBuilder), rightOperand.calculate(contextStack, stringBuilder));
        if(!negative){
            return result;
        } else {
            return -result;
        }
    }

    @Override
    public void changeNegative() {
        if (!negative) {
            negative = true;
        } else {
            negative = false;
        }
    }
}
