package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

import java.util.List;

/**
 * Created by mmudel on 08.06.2016.
 */
public class ArithmeticFuncCall implements IArithmeticExpression {

    public ArithmeticFuncCall(Identifier identifier, List<IValue> args) {
        this.identifier = identifier;
        this.args = args;
        this.negative = false;
    }

    private boolean negative;
    private Identifier identifier;
    private List<IValue> args;

    @Override
    public double calculate(ContextStack contextStack, StringBuilder stringBuilder) {
        double result = contextStack.findFunction(identifier).execute(contextStack, stringBuilder, args);
        if(!negative){
            return result;
        } else {
            return -result;
        }
    }

    @Override
    public void changeNegative() {
        if (!negative) {
            negative = true;
        } else {
            negative = false;
        }
    }
}
