package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

/**
 * Created by mmudel on 09.06.2016.
 */
public class Assignment implements ICommand {

    public Assignment(Identifier identifier, IArithmeticExpression arithmeticExpression) {
        this.identifier = identifier;
        this.arithmeticExpression = arithmeticExpression;
    }

    private Identifier identifier;
    private IArithmeticExpression arithmeticExpression;

    @Override
    public void generate(ContextStack contextStack, StringBuilder stringBuilder) {
        contextStack.addValue(identifier, new Value(arithmeticExpression.calculate(contextStack, stringBuilder)));
    }
}
