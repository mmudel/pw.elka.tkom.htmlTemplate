package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.Context;
import pw.elka.tkom.parser.Context.ContextStack;
import pw.elka.tkom.parser.tokens.ComparisonOperator;

/**
 * Created by mmudel on 29.05.2016.
 */
public class BooleanExpression {

    public BooleanExpression(IArithmeticExpression leftOperand, IArithmeticExpression rightOperand, ComparisonOperator operator){
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
        this.operator = operator;
    }

    private IArithmeticExpression leftOperand;
    private IArithmeticExpression rightOperand;
    private ComparisonOperator operator;

    public boolean calculate(ContextStack contextStack, StringBuilder stringBuilder){
        return this.operator.compare(leftOperand.calculate(contextStack, stringBuilder), rightOperand.calculate(contextStack, stringBuilder));
    }
}
