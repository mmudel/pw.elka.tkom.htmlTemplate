package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

/**
 * Created by mmudel on 2016-06-08.
 */
public class CommandForeach implements ICommand {

    public CommandForeach(CommandList commandList, Identifier first, JsonObject jsonObject) {
        this.commandList = commandList;
        this.first = first;
        this.jsonObject = jsonObject;
    }

    private CommandList commandList;
    private Identifier first;
    private JsonObject jsonObject; // obiekt powinien implementowac iterator z opcja iterowania po kolejnych obiektach z jsona

    @Override
    public void generate(ContextStack contextStack, StringBuilder stringBuilder) { // jak chodzic po obiektach klasy? predefiniowane klasy? getClass().getName() ? jak sprawdzac dla kazdej istniejacej klasy?
        commandList.generate(contextStack, stringBuilder);
    }
}
