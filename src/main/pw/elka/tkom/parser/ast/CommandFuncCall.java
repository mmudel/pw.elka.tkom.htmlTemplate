package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

import java.util.List;

/**
 * Created by mmudel on 12.06.2016.
 */
public class CommandFuncCall implements ICommand {

    public CommandFuncCall(Identifier identifier, List<IValue> args) {
        this.identifier = identifier;
        this.args = args;
    }

    private Identifier identifier;
    private List<IValue> args;

    @Override
    public void generate(ContextStack contextStack, StringBuilder stringBuilder) {
        contextStack.findFunction(identifier).execute(contextStack, stringBuilder, args);
        return;
    }
}
