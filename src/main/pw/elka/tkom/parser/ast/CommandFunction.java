package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.Context;
import pw.elka.tkom.parser.Context.ContextStack;

import java.util.List;

/**
 * Created by mmudel on 09.06.2016.
 */
public class CommandFunction implements ICommand {

    public CommandFunction(Identifier identifier, List<Identifier> arguments, CommandList commandList, NumericValue returnValue) {
        this.identifier = identifier;
        this.arguments = arguments;
        this.commandList = commandList;
        this.returnValue = returnValue;
    }

    private Identifier identifier;
    private List<Identifier> arguments;
    private CommandList commandList;
    private NumericValue returnValue;

    @Override
    public void generate(ContextStack contextStack, StringBuilder stringBuilder) {
        contextStack.addFunction(identifier, this);
    }

    public double execute(ContextStack contextStack, StringBuilder stringBuilder, List<IValue> args){
        double result;
        contextStack.addScope();
        for (int i = 0; i < arguments.size(); i++) {
            contextStack.addValue(arguments.get(i), args.get(i));
        }
        commandList.generateWithContextStack(contextStack, stringBuilder);
        if(returnValue != null)
            result = returnValue.calculate(contextStack, stringBuilder);
        else
            result = 0.0;
        contextStack.deleteScope();
        contextStack.deleteScope();
        return result;
    }
}
