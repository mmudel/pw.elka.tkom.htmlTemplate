package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

/**
 * Created by mmudel on 30.05.2016.
 */
public class CommandIf implements ICommand {

    public CommandIf(BooleanExpression booleanExpression, CommandList commandList) {
        this.booleanExpression = booleanExpression;
        this.commands = commandList;
    }

    private BooleanExpression booleanExpression;
    private CommandList commands;

    @Override
    public void generate(ContextStack contextStack, StringBuilder stringBuilder) {
        if (booleanExpression.calculate(contextStack, stringBuilder)) {
            commands.generate(contextStack, stringBuilder);
        }
    }
}
