package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

import java.util.List;

/**
 * Created by mmudel on 08.06.2016.
 */
public class CommandList implements ICommand {
    public CommandList(List<ICommand> commandList) {
        this.commandList = commandList;
    }

    private List<ICommand> commandList;

    @Override
    public void generate(ContextStack contextStack, StringBuilder stringBuilder) {
        contextStack.addScope();
        for (ICommand command : commandList) {
            command.generate(contextStack, stringBuilder);
        }
        contextStack.deleteScope();
    }

    public void generateWithContextStack(ContextStack contextStack, StringBuilder stringBuilder){
        contextStack.addScope();
        for (ICommand command : commandList) {
            command.generate(contextStack, stringBuilder);
        }
    }
}
