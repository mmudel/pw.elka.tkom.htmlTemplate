package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

import java.util.List;

/**
 * Created by mmudel on 09.06.2016.
 */
public class CommandPrint implements ICommand {

    public CommandPrint(List<IValue> values) {
        this.values = values;
    }

    private List<IValue> values;

    @Override
    public void generate(ContextStack contextStack, StringBuilder stringBuilder) {
        for (IValue value : values) {
            if (value.isString()) {
                stringBuilder.append(value.getStringValue());
            } else if (value.isNumeric()) {
                stringBuilder.append(value.getDoubleValue().toString());
            } else {
                IValue value1 = contextStack.findValue(value.getJsonObject().toIdentifier());
                boolean numeric = value1.isNumeric();
                if (numeric) {
                    stringBuilder.append(value1.getDoubleValue());
                } else {
                    if (value1.isString())
                        stringBuilder.append(value1.getStringValue());
                    else {
                        IValue value2 = contextStack.findValue(value1.getJsonObject().toIdentifier());
                        boolean numeric2 = value2.isNumeric();
                        if(numeric2) {
                            stringBuilder.append(value2.getDoubleValue());
                        }
                        else{
                            stringBuilder.append(value2.getStringValue());
                        }
                    }
                }
            }
        }
    }
}
