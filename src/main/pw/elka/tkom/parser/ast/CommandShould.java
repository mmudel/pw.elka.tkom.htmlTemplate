package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

/**
 * Created by mmudel on 29.05.2016.
 */
public class CommandShould implements ICommand {

    public CommandShould(BooleanExpression booleanExpression, Identifier identifier) {
        this.booleanExpression = booleanExpression;
        this.identifier = identifier;
    }

    private BooleanExpression booleanExpression;
    private Identifier identifier;

    @Override
    public void generate(ContextStack contextStack, StringBuilder stringBuilder) {  // co mialbym tu zwrocic?

    }


    public BooleanExpression getBooleanExpression() {
        return booleanExpression;
    }

    public Identifier getIdentifier() {
        return identifier;
    }
}
