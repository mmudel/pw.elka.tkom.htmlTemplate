package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

/**
 * Created by mmudel on 30.05.2016.
 */
public interface ICommand {
    void generate(ContextStack contextStack, StringBuilder stringBuilder);
}