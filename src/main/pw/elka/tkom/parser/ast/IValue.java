package pw.elka.tkom.parser.ast;

/**
 * Created by mmudel on 09.06.2016.
 */
public interface IValue {

    public boolean isNumeric();

    public boolean isString();

    public boolean isJson();

    public String getStringValue();

    public Double getDoubleValue();

    public JsonObject getJsonObject();
}
