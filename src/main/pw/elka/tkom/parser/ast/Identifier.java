package pw.elka.tkom.parser.ast;

/**
 * Created by mmudel on 29.05.2016.
 */
public class Identifier {

    public Identifier(String value){
        this.value = value;
    }

    private String value;   // musi byc mapa z kluczem jako tym value i wartosciami

    public String getValue() {
        return value;
    }
}
