package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.Context;

import java.util.List;

/**
 * Created by mmudel on 2016-06-08.
 */
public class JsonObject {

    public JsonObject(List<Identifier> identifiers) {
        this.identifiers = identifiers;
    }

    private List<Identifier> identifiers;

    public Identifier toIdentifier(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(identifiers.get(0).getValue());
        for (int i = 1; i < identifiers.size(); i++) {
            stringBuilder.append(".");
            stringBuilder.append(identifiers.get(1).getValue());
        }
        return new Identifier(stringBuilder.toString());
    }

    public double getNumericValue(Context context){
        return 0;
    }
}
