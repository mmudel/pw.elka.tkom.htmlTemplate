package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

/**
 * Created by mmudel on 08.06.2016.
 */
public class NumericValue implements IArithmeticExpression, IValue {

    public NumericValue(Double doubleValue) {
        this.doubleValue = doubleValue;
        this.jsonObject = null;
        this.negative = false;
    }

    public NumericValue(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
        this.doubleValue = null;
        this.negative = false;
    }

    private boolean negative;
    private Double doubleValue;
    private JsonObject jsonObject;

    public boolean isNumeric(){
        return doubleValue!=null;
    }

    public boolean isJson(){
        return jsonObject!=null;
    }

    public Double getDoubleValue() {
        return doubleValue;
    }

    public JsonObject getJsonObject() {
        return jsonObject;
    }

    @Override
    public boolean isString() {
        return false;
    }

    @Override
    public String getStringValue() {
        return null;
    }

    @Override
    public double calculate(ContextStack contextStack, StringBuilder stringBuilder) {
        double result;
        if (doubleValue != null) {
            result = doubleValue;
        } else {
            IValue value1 = contextStack.findValue(jsonObject.toIdentifier());
            if(value1.isNumeric()) {
                result = contextStack.findValue(jsonObject.toIdentifier()).getDoubleValue();
            } else {
                result = contextStack.findValue(value1.getJsonObject().toIdentifier()).getDoubleValue();
            }
        }
        if(!negative){
            return result;
        } else {
            return -result;
        }
    }

    @Override
    public void changeNegative() {
        if (!negative) {
            negative = true;
        } else {
            negative = false;
        }
    }
}
