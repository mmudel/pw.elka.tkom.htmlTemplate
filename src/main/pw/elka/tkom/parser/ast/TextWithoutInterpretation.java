package pw.elka.tkom.parser.ast;

import pw.elka.tkom.parser.Context.ContextStack;

/**
 * Created by mmudel on 09.06.2016.
 */
public class TextWithoutInterpretation implements ICommand {


    public TextWithoutInterpretation(String value) {
        this.value = value;
    }

    private String value;

    @Override
    public void generate(ContextStack contextStack, StringBuilder stringBuilder) {
        stringBuilder.append(value);
    }
}
