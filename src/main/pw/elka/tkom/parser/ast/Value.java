package pw.elka.tkom.parser.ast;

/**
 * Created by mmudel on 12.06.2016.
 */
public class Value implements IValue {

    public Value(String stringValue) {
        this.stringValue = stringValue;
        this.jsonObject = null;
        this.doubleValue = null;
    }

    public Value(Double doubleValue) {
        this.doubleValue = doubleValue;
        this.stringValue = null;
        this.jsonObject = null;
    }

    public Value(JsonObject jsonObject) {
        this.jsonObject = jsonObject;
        this.stringValue = null;
        this.doubleValue = null;
    }

    private String stringValue = null;
    private Double doubleValue = null;
    private JsonObject jsonObject = null;

    public boolean isNumeric() {
        return doubleValue != null;
    }

    public boolean isString() {
        return stringValue != null;
    }

    public boolean isJson() {
        return jsonObject != null;
    }

    public String getStringValue() {
        return stringValue;
    }

    public Double getDoubleValue() {
        return doubleValue;
    }

    public JsonObject getJsonObject() {
        return jsonObject;
    }
}
