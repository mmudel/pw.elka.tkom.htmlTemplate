package pw.elka.tkom.parser.exceptions;

/**
 * Created by mmudel on 29.05.2016.
 */
public class UnexpectedInputException extends Exception {
    public UnexpectedInputException() { super(); }
    public UnexpectedInputException(String message) { super(message); }
    public UnexpectedInputException(String message, Throwable cause) { super(message, cause); }
    public UnexpectedInputException(Throwable cause) { super(cause); }
}
