package pw.elka.tkom.parser.exceptions;

/**
 * Created by mmudel on 29.05.2016.
 */
public class UnknownTokenException extends Exception{
    public UnknownTokenException() { super(); }
    public UnknownTokenException(String message) { super(message); }
    public UnknownTokenException(String message, Throwable cause) { super(message, cause); }
    public UnknownTokenException(Throwable cause) { super(cause); }
}
