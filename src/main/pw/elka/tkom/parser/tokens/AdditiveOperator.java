package pw.elka.tkom.parser.tokens;

public enum AdditiveOperator {
    Additive {
        @Override
        public double calculate(double a, double b) {
            return a + b;
        }
    },
    Subtractive {
        @Override
        public double calculate(double a, double b) {
            return a - b;
        }
    };

    public abstract double calculate(double a, double b);
}
