package pw.elka.tkom.parser.tokens;

public enum ComparisonOperator {
    IsEqual {
        @Override
        public boolean compare(double a, double b) {
            return a == b;
        }
    },
    IsBigger {
        @Override
        public boolean compare(double a, double b) {
            return a > b;
        }
    },
    IsBiggerEqual{
        @Override
        public boolean compare(double a, double b) {
            return a >= b;
        }
    },
    IsLower{
        @Override
        public boolean compare(double a, double b) {
            return a < b;
        }
    },
    IsLowerEqual{
        @Override
        public boolean compare(double a, double b) {
            return a <= b;
        }
    };

    public abstract boolean compare(double a, double b);
}
