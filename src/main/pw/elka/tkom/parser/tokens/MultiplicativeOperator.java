package pw.elka.tkom.parser.tokens;

public enum MultiplicativeOperator{
    Multiplicative{
        @Override
        public double calculate(double a, double b) {
            return a*b;
        }
    },
    Divisional{
        @Override
        public double calculate(double a, double b) {
            return a/b;
        }
    };

    public abstract double calculate(double a, double b);
}
