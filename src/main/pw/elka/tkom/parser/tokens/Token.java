package pw.elka.tkom.parser.tokens;

/**
 * Created by mmudel on 10.05.2016.
 */
public class Token { // @TODO zamienic na klase a wewnatrz zrobic enum
    public enum TokenType {
        OpenCommand,
        CloseCommand,
        Should,
        If,
        Endif,
        Foreach,
        In,
        Endfor,
        Function,
        Endfunction,
        Print,
        Coma,
        Dot,
        OpeningParenthesis,
        ClosingParenthesis,
        Identifier,
        ComparisonOperator,
        AssignmentOperator,
        NumericValue,
        StringValue,
        AdditiveOperator,
        MultiplicativeOperator,
        TextWithoutInterpretation,
        EOF,
        Minus,
        Return;
    }

    double numberValue;
    String stringValue;
    AdditiveOperator additiveOperator;
    MultiplicativeOperator multiplicativeOperator;
    ComparisonOperator comparisonOperator;

    public AdditiveOperator getAdditiveOperator() {
        return additiveOperator;
    }

    public void setAdditiveOperator(AdditiveOperator additiveOperator) {
        this.additiveOperator = additiveOperator;
    }

    public MultiplicativeOperator getMultiplicativeOperator() {
        return multiplicativeOperator;
    }

    public void setMultiplicativeOperator(MultiplicativeOperator multiplicativeOperator) {
        this.multiplicativeOperator = multiplicativeOperator;
    }

    public ComparisonOperator getComparisonOperator() {
        return comparisonOperator;
    }

    public void setComparisonOperator(ComparisonOperator comparisonOperator) {
        this.comparisonOperator = comparisonOperator;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public double getNumberValue() {

        return numberValue;
    }

    public void setNumberValue(double numberValue) {
        this.numberValue = numberValue;
    }

    public TokenType tokenType;

    public Token(TokenType tokenType){
        this.tokenType = tokenType;
    }

}

