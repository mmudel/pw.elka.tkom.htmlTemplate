package pw.elka.tkom.parser;

import org.junit.Test;
import pw.elka.tkom.parser.ast.CommandList;

import static org.junit.Assert.assertEquals;

import java.io.StringReader;

/**
 * Created by mmudel on 09.06.2016.
 */
public class ParserTest {

    private Scanner scanner;
    private Parser parser;

    private void createScanner(String text) {
        StringReader stringReader = new StringReader(text);
        this.scanner = new Scanner(stringReader);
    }

    private void createScannerInCommand(String text) {
        StringReader stringReader = new StringReader(text);
        this.scanner = new Scanner(stringReader);
        this.scanner.inCommandMode = true;
    }

    @Test
    public void parse_Normal_ExpectedCommandList() throws Exception {
        createScanner("<body>\n" +
                "<table>\n" +
                "{$ foreach(employee in company.employees) $}\n" +
                "<tr>\n" +
                "\t<td>{$ print(employee.name) $}</td>\n" +
                "\t<td>{$ print(employee.surname) $}</td>\n" +
                "\t<td>{$ print(employee.age) $}</td>\n" +
                "\t{$ if(employee.haveSecondNumber == 1) $}\n" +
                "\t\t<td>{$ print(employee.secondNumber) $}</td>\n" +
                "\t{$ endif $}\n" +
                "</tr>\n" +
                "{$ endfor $}\n" +
                "</table>\n" +
                "</body>");
        parser = new Parser(scanner);
        CommandList commandList = parser.parse();
    }

    @Test
    public void run_CommandIfTrue_SimpleHtmlFileGenerated() throws Exception {
        createScanner("<html>\n{$ something = 4 $}\n" +
                "{$ if(something == 4) $}\n" +
                "<body> <//body>\n" +
                "{$ endif $}\n" +
                "<//html>");
        parser = new Parser(scanner);
        parser.run();
        assertEquals("<html>\n\n\n<body> </body>\n\n</html>", parser.generatedFile);
    }

    @Test
    public void run_CommandIfFalse_SimpleHtmlFileGenerated() throws Exception {
        createScanner("<html>\n{$ something = 4 $}\n" +
                "{$ if(something == 5) $}\n" +
                "<body> <//body>\n" +
                "{$ endif $}\n" +
                "<//html>");
        parser = new Parser(scanner);
        parser.run();
        assertEquals("<html>\n\n\n</html>", parser.generatedFile);
    }

    @Test
    public void run_ArithmeticAndCommandIfTrue_SimpleHtmlFileGenerated() throws Exception {
        createScanner("<html>\n{$ something = 4 + 5 $}{$ something2 = something + 1 $}\n" +
                "{$ if(something2 == 10) $}\n" +
                "<body> <//body>\n" +
                "{$ endif $}\n" +
                "<//html>");
        parser = new Parser(scanner);
        parser.run();
        assertEquals("<html>\n\n\n<body> </body>\n\n</html>", parser.generatedFile);
    }

    @Test
    public void run_Arithmetic2AndCommandIfTrue_SimpleHtmlFileGenerated() throws Exception {
        createScanner("<html>\n{$ something = 4 + 5 $}{$ something2 = something * 3 $}\n" +
                "{$ if(something2 == 27) $}\n" +
                "<body> <//body>\n" +
                "{$ endif $}\n" +
                "<//html>");
        parser = new Parser(scanner);
        parser.run();
        assertEquals("<html>\n\n\n<body> </body>\n\n</html>", parser.generatedFile);
    }

    @Test
    public void run_CommandFunctionReturnVoidIgnoreArgs_SimpleHtmlFileGenerated() throws Exception {
        createScanner("{$ function funkcja(id1, id2) $} <html> <body> <//body> <//html> {$ return 1 $}" +
                "{$ funkcja(1, 2) $}");
        parser = new Parser(scanner);
        parser.run();
        assertEquals(" <html> <body> </body> </html> ", parser.generatedFile);
    }

    @Test
    public void run_CommandFunctionReturnVoidIgnoreArgsPrintString_SimpleHtmlFileGenerated() throws Exception {
        createScanner("{$ function funkcja(id1, id2) $} <html> <body> {$ print(\"something\") $} <//body> <//html> {$ return 1 $}" +
                "{$ funkcja(1, 2) $}");
        parser = new Parser(scanner);
        parser.run();
        assertEquals(" <html> <body> something </body> </html> ", parser.generatedFile);
    }

    @Test
    public void run_CommandFunctionReturnVoidIgnoreArgsPrintNumber_SimpleHtmlFileGenerated() throws Exception {
        createScanner("{$ function funkcja(id1, id2) $} <html> <body> {$ print(2) $} <//body> <//html> {$ return $}" +
                "{$ funkcja(1, 2) $}");
        parser = new Parser(scanner);
        parser.run();
        assertEquals(" <html> <body> 2.0 </body> </html> ", parser.generatedFile);
    }

    @Test
    public void run_CommandFunctionReturnVoidArgsUsedPrintArgNumber_SimpleHtmlFileGenerated() throws Exception {
        createScanner("{$ function funkcja(id1, id2) $} <html> <body> {$ print(id1) $} <//body> <//html> {$ return 1 $}" +
                "{$ funkcja(1, 2) $}");
        parser = new Parser(scanner);
        parser.run();
        assertEquals(" <html> <body> 1.0 </body> </html> ", parser.generatedFile);
    }

    @Test
    public void run_CommandFunctionReturnVoidArgsUsedPrintArgString_SimpleHtmlFileGenerated() throws Exception {
        createScanner("{$ function funkcja(id1, id2) $} <html> <body> {$ print(id1) $} <//body> <//html> {$ return 1 $}" +
                "{$ funkcja(\"something\", 2) $}");
        parser = new Parser(scanner);
        parser.run();
        assertEquals(" <html> <body> something </body> </html> ", parser.generatedFile);
    }

    @Test
    public void run_CommandFunctionReturnVoidArgsUsedPrintArgIdentifier_SimpleHtmlFileGenerated() throws Exception {
        createScanner("{$ myIdentifier1 = 4 $}{$ function funkcja(id1, id2) $} <html> <body> {$ print(id1) $} <//body> <//html> {$ return 1 $}" +
                "{$ funkcja(myIdentifier1, 2) $}");
        parser = new Parser(scanner);
        parser.run();
        assertEquals(" <html> <body> 4.0 </body> </html> ", parser.generatedFile);
    }

    @Test
    public void run_CommandFunctionReturnNumberArgsUsedPrintArgIdentifier_SimpleHtmlFileGenerated() throws Exception {
        createScanner("{$ myIdentifier1 = 4 $}{$ myIdentifier2 = 8 $}{$ function funkcja(id1, id2) $}{$ id3 = id1 + id2 $}{$ return id3 $}" +
                "{$ myIdentifier3 = 1 + funkcja(myIdentifier1, myIdentifier2) $}{$ if(myIdentifier3 == 13) $}<body> <//body> {$ endif $}");
        parser = new Parser(scanner);
        parser.run();
        assertEquals("<body> </body> ", parser.generatedFile);
    }

    @Test
    public void run_TestContextStacks_SimpleHtmlFileGenerated() throws Exception {
        createScanner("<html>\n{$ something = 4 + 5 $}{$ something2 = something + 1 $}\n" +
                "{$ if(something2 == 10) $}\n" +
                "{$ something2 = 15 $}\n" +
                "{$ if(something2 == 10) $}\n" +
                "<td>cos<//td>\n" +
                "{$ endif $}\n" +
                "{$ if(something2 == 15) $}\n" +
                "<td>cos innego<//td>\n" +
                "{$ endif $}\n" +
                "{$ endif $}\n" +
                "<//html>");
        parser = new Parser(scanner);
        parser.run();
        assertEquals("<html>\n\n\n\n\n\n<td>cos innego</td>\n\n\n</html>", parser.generatedFile);
    }
}
