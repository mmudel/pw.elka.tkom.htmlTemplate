package pw.elka.tkom.parser;

import org.junit.Before;
import org.junit.Test;
import pw.elka.tkom.parser.exceptions.UnknownTokenException;
import pw.elka.tkom.parser.tokens.ComparisonOperator;
import pw.elka.tkom.parser.tokens.Token;

import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by mmudel on 16.05.2016.
 */
public class ScannerTest {

    private Scanner scanner;

    private void createScanner(String text) {
        StringReader stringReader = new StringReader(text);
        this.scanner = new Scanner(stringReader);
    }

    private void createScannerInCommand(String text) {
        StringReader stringReader = new StringReader(text);
        this.scanner = new Scanner(stringReader);
        this.scanner.inCommandMode = true;
    }

    private Token scanNext() throws Exception {
        this.scanner.readNextToken();
        return this.scanner.getToken();
    }

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void ignoreWhitespaces_TextWithWhitespaces_ExpectedResult() throws Exception {
        /// Given
        int result;
        int expectedResult = 'T';
        StringReader stringReader = new StringReader("  Text");
        Scanner scanner = new Scanner(stringReader);
        /// When
        scanner.inCommandMode = true;
        scanner.ignoreWhitespaces();
        result = scanner.reader.read();
        /// Then
        assertEquals(expectedResult, result);
    }

    @Test
    public void readNextToken_UnknownToken_CorrectLineNumber() throws Exception {
        /// Given
        this.createScannerInCommand(" \n  \n  \n ident \n $} text text \n text \n {$ [] ");
        /// When
        try {
            this.scanNext();
            this.scanNext();
            this.scanNext();
            this.scanNext();
            this.scanNext();
        }
        /// Then
        catch (UnknownTokenException e) {
            assertEquals("Unknown token [ at line 7", e.getMessage());
        }
    }

    @Test
    public void readNextToken_EmptyString_TokenEOF() throws Exception {
        /// Given
        this.createScannerInCommand("");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.EOF, result.tokenType);
    }

    @Test
    public void readNextToken_OpenCommandString_TokenOpenCommand() throws Exception {
        /// Given
        this.createScanner("{$ text $}");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.OpenCommand, result.tokenType);
    }

    @Test(expected = UnknownTokenException.class)
    public void readNextToken_OpenCommandStringInCommandMode_Exception() throws Exception {
        /// Given
        this.createScannerInCommand("{$ text $}");
        /// When
        this.scanNext();
        /// Then expect exception
    }

    @Test
    public void readNextToken_OpenCommandStringWithSlash_TokenTextWithoutInterpretation() throws Exception {
        /// Given
        this.createScanner("/{$ text $}");
        /// When
        Token result = this.scanNext();
        String stringResult = result.getStringValue();
        /// Then
        assertEquals(Token.TokenType.TextWithoutInterpretation, result.tokenType);
        assertEquals("{$ text $}", stringResult);
    }

    @Test
    public void readNextToken_CloseCommand_TokenCloseCommand() throws Exception {
        /// Given
        this.createScannerInCommand("$}");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.CloseCommand, result.tokenType);
    }

    @Test
    public void readNextToken_CommandIf_TokenIf() throws Exception {
        /// Given
        this.createScannerInCommand("if(");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.If, result.tokenType);
    }

    @Test
    public void readNextToken_CommandForeach_TokenForeach() throws Exception {
        /// Given
        this.createScannerInCommand("foreach");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.Foreach, result.tokenType);
    }

    @Test
    public void readNextToken_CommandEndfunction_TokenEndfunction() throws Exception {
        /// Given
        this.createScannerInCommand("endfunction");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.Endfunction, result.tokenType);
    }

    @Test
    public void readNextToken_Identifier_TokenIdentifier() throws Exception {
        /// Given
        this.createScannerInCommand("MyIdentifier ");
        /// When
        Token result = this.scanNext();
        String resultValue = result.getStringValue();
        /// Then
        assertEquals(Token.TokenType.Identifier, result.tokenType);
        assertEquals("MyIdentifier", resultValue);
    }

    @Test
    public void readNextToken_ComparisonOperatorBiggerEqual_TokenIsBiggerEqual() throws Exception {
        /// Given
        this.createScannerInCommand(">=");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.ComparisonOperator, result.tokenType);
        assertEquals(ComparisonOperator.IsBiggerEqual, result.getComparisonOperator());
    }

    @Test
    public void readNextToken_ComparisonOperatorLower_TokenIsLower() throws Exception {
        /// Given
        this.createScannerInCommand("<");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.ComparisonOperator, result.tokenType);
        assertEquals(ComparisonOperator.IsLower, result.getComparisonOperator());

    }

    @Test
    public void readNextToken_ComparisonOperatorEqual_TokenIsEqual() throws Exception {
        /// Given
        this.createScannerInCommand("==");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.ComparisonOperator, result.tokenType);
        assertEquals(ComparisonOperator.IsEqual, result.getComparisonOperator());

    }

    @Test
    public void readNextToken_NumericValue_TokenNumericValue() throws Exception {
        /// Given
        this.createScannerInCommand("37");
        /// When
        Token result = this.scanNext();
        double numericResult = result.getNumberValue();
        /// Then
        assertEquals(Token.TokenType.NumericValue, result.tokenType);
        assertTrue(numericResult == 37);
    }

    @Test
    public void readNextToken_StringValue_TokenStringValue() throws Exception {
        /// Given
        this.createScannerInCommand("\"SomeStringValue\"");
        /// When
        Token result = this.scanNext();
        String stringResult = result.getStringValue();
        /// Then
        assertEquals(Token.TokenType.StringValue, result.tokenType);
        assertEquals("SomeStringValue", stringResult);
    }

    @Test
    public void readNextToken_AdditiveOperator_TokenAdditiveOperator() throws Exception {
        /// Given
        this.createScannerInCommand("+");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.AdditiveOperator, result.tokenType);
    }

    @Test
    public void readNextToken_MultiplicativeOperator_TokenMultiplicativeOperator() throws Exception {
        /// Given
        this.createScannerInCommand("*");
        /// When
        Token result = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.MultiplicativeOperator, result.tokenType);
    }

    @Test
    public void readNextToken_TextWithoutInterpretation_TokenTextWithoutInterpretation() throws Exception {
        /// Given
        StringReader stringReader = new StringReader("Some text that shouldnt been interpreted \n {$");
        this.createScanner("Some text that shouldnt been interpreted \n {$");
        /// When
        Token result = this.scanNext();
        String stringResult = result.getStringValue();
        /// Then
        assertEquals(Token.TokenType.TextWithoutInterpretation, result.tokenType);
        assertEquals("Some text that shouldnt been interpreted \n ", stringResult);
    }

    @Test
    public void readNextToken_BiggerStringTest_ExpectedTokens() throws Exception {
        /// Given
        this.createScanner("{$ should( age>18, wrongAge ) $}");
        /// When
        Token result1 = this.scanNext();
        Token result2 = this.scanNext();
        Token result3 = this.scanNext();
        Token result4 = this.scanNext();
        String stringValue1 = result4.getStringValue();
        Token result5 = this.scanNext();
        Token result6 = this.scanNext();
        Token result7 = this.scanNext();
        Token result8 = this.scanNext();
        String stringValue2 = result8.getStringValue();
        Token result9 = this.scanNext();
        Token result10 = this.scanNext();
        /// Then
        assertEquals(Token.TokenType.OpenCommand, result1.tokenType);
        assertEquals(Token.TokenType.Should, result2.tokenType);
        assertEquals(Token.TokenType.OpeningParenthesis, result3.tokenType);
        assertEquals(Token.TokenType.Identifier, result4.tokenType);
        assertEquals(Token.TokenType.ComparisonOperator, result5.tokenType);
        assertEquals(Token.TokenType.NumericValue, result6.tokenType);
        assertEquals(Token.TokenType.Coma, result7.tokenType);
        assertEquals(Token.TokenType.Identifier, result8.tokenType);
        assertEquals(Token.TokenType.ClosingParenthesis, result9.tokenType);
        assertEquals(Token.TokenType.CloseCommand, result10.tokenType);
        assertEquals("age", stringValue1);
        assertEquals("wrongAge", stringValue2);
    }
}

/* pw.elka.tkom.parser.Parser

 */